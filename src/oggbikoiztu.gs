// usa la libreria gtk
uses
	Gtk
	Gst
	Gee

// comienza la parte principal del programa
ventana_principal:Ventana
combine:bool=true
directorio_datos:string
t:Traduccion
init
	Gtk.init (ref args)               // inicializa gtk
	Gst.init (ref args);
	var directorio_traduccion="/usr/share/oggbikoiztu"
	//var directorio_traduccion="/home/gontzal/Mahaigaina/oggbikoiztu/oggbikoiztu-1.0"
	t= new Traduccion(directorio_traduccion,"es") //Especifica el idioma en el que esta escrito
	t.traduce()
	ventana_principal = new Ventana ()      // crea el objeto ventana_principal
	ventana_principal.show_all ()                  // muestra todo
	ventana_principal.scale_1.hide()
	ventana_principal.boton_borrartodo.set_sensitive(false)
	ventana_principal.boton_borrartrozo.set_sensitive(false)
	ventana_principal.boton_grabar.set_sensitive(false)
	ventana_principal.boton_crearvideo.set_sensitive(false)
	ventana_principal.boton_play.set_sensitive(false)
	ventana_principal.boton_parar.set_sensitive(false)
	ventana_principal.boton_opciones.set_sensitive(true)
	Gtk.main ();                      // comienza con el loop

class Ventana : Window             // Crea una clase de ventana
	
	dialogo_silenciar:Dialogo_silenciar
	dialogo_opciones:Dialogo_opciones
	
	drawing_area: DrawingArea;
	videopipeline:  Pipeline ;
	vaudiopipeline:  Pipeline;
	audiopipeline:  Pipeline ;
	
	videosrc : Element;
	videosink:  Element;
	
	vaudiosrc:  Element;
	vaudiosink:  Element;
	vaudiocon:  Element;
	vaudiodemux:  Element;
	vaudiodecoder:  Element;
	
	audiosrc : Element;
	audiosink:  Element;
	audioconvert:  Element;
	audioencoder:  Element;
	
	comienzo_grabacion:float
	nombre_archivo:string
	archivo_abierto:bool
	xid :ulong ;
	reloj:uint
	position: float
	duracion:float
	bus:Gst.Bus
	bus2:Gst.Bus
	msg:Gst.Message
	msg2:Gst.Message
	seek_enabled:bool
	seek_enabled2:bool
	scale_1:Scale
	estado:string
	lugar:int
	boton_grabar:Button
	boton_parar:Button
	boton_play:Button
	boton_abrir:Button
	boton_borrartodo:Button
	boton_crearvideo:Button
	boton_borrartrozo:Button
	boton_opciones:Button
	box:Box
	
	init
		
		verificar_directorio_usuario (GLib.Environment.get_home_dir())
		reloj = Timeout.add(100, mover)
		title = "OggBikoiztu"          // escribe el titulo
		default_height = 250                // anchura
		default_width = 450                  // altura
		window_position = WindowPosition.MOUSE // posición
		comienzo_grabacion=-1
		nombre_archivo=""
		archivo_abierto=true
		dialogo_silenciar= new Dialogo_silenciar()
		dialogo_opciones= new Dialogo_opciones()

		// video pipeline 
		duracion=Gst.CLOCK_TIME_NONE;
		this.videopipeline = new Pipeline ("mypipeline");
		this.videosrc = ElementFactory.make ("playbin2", "playbin2")
		this.videosrc.set_property("volume", 0)
		this.videosink = ElementFactory.make ("xvimagesink", "xvimagesink");
		this.videosrc.set("video-sink",this.videosink);
		this.videosink.set_property("force-aspect-ratio", true)
		this.videopipeline.add_many (this.videosrc)

		// video audio pipeline: 
		this.vaudiopipeline=new Pipeline("vaudiopipeline")
		this.vaudiosrc = ElementFactory.make ("filesrc", "filesrc");
		this.vaudiosrc.set ("location",directorio_datos+"archivo_audio.wav")
		this.vaudiodemux= ElementFactory.make ("decodebin2", "decodebin2");
		this.vaudiodecoder= ElementFactory.make ("audioconvert", "audioconvert");
		this.vaudiocon= ElementFactory.make ("audioresample", "audioresample");
		this.vaudiosink= ElementFactory.make ("alsasink", "alsasink");
		this.vaudiodemux.pad_added.connect(Onpad);
		this.vaudiopipeline.add_many(this.vaudiosrc, vaudiodemux, vaudiodecoder, vaudiocon,vaudiosink)
		this.vaudiosrc.link_many(this.vaudiodemux)
		this.vaudiodecoder.link_many(this.vaudiocon,this.vaudiosink)
		
		// audio recorder pipeline: gst-launch pulsesrc ! audioconvert ! vorbisenc ! oggmux ! filesink location=dump.ogg
		this.audiopipeline = new Pipeline ("audiopipeline");
		this.audiosrc= ElementFactory.make ("alsasrc","alsasrc")
		this.audioconvert=ElementFactory.make ("audioconvert","audioconvert")
		this.audioencoder = Gst.ElementFactory.make("wavenc", "wavenc")
		this.audiosink= ElementFactory.make ("filesink","filesink")
		this.audiosink.set ("location",directorio_datos+"grabacion.wav")
		this.audiopipeline.add_many (this.audiosrc,this.audioconvert, this.audioencoder,this.audiosink);
		this.audiosrc.link_many(this.audioconvert,audioencoder,audiosink)
		
		// creamos un boton con la siguiente etiqueta
		boton_grabar = new Button.with_label (t.t("Grabar"))
		boton_parar = new Button.with_label (t.t("Parar"))
		boton_play = new Button.with_label (t.t("Play"))
		boton_abrir = new Button.with_label (t.t("Abrir"))
		boton_borrartodo = new Button.with_label (t.t("Silencia todo"))
		boton_borrartrozo = new Button.with_label (t.t("Silencia trozo"))
		boton_crearvideo = new Button.with_label (t.t("Crea video"))
		boton_opciones = new Button.with_label (t.t("Opciones"))
		
		box= new Box (Gtk.Orientation.HORIZONTAL, 4)
		var box1= new Box (Gtk.Orientation.HORIZONTAL, 4)
		var box2= new Box (Gtk.Orientation.VERTICAL, 4)
		
		
		scale_1=  new Scale.with_range (Orientation.HORIZONTAL,0,1,0.1)
		scale_1.value_changed.connect(on_scale)
		
		this.drawing_area = new DrawingArea ();
		this.drawing_area.realize.connect(on_realize);
		
		// Une el evento de clic de raton con la funcion pulsado
		boton_abrir.clicked.connect (on_abrir)
		boton_grabar.clicked.connect (on_grabar)
		boton_parar.clicked.connect (on_parar)
		boton_play.clicked.connect (on_play)
		boton_borrartodo.clicked.connect (on_borrartodo)
		boton_crearvideo.clicked.connect (on_crea_video)
		boton_borrartrozo.clicked.connect (on_borrartrozo)
		boton_opciones.clicked.connect (on_opciones)

		// si pulsamos la x de la barra saldrá del loop
		destroy.connect(Gtk.main_quit)
		
		//teclado
		this.box.add_events(Gdk.EventMask.KEY_PRESS_MASK)
		this.box.key_press_event.connect(on_tecla)
 
		// añade el boton a la ventana
		this.add(box2)
		box.add(boton_abrir)
		box.add(boton_play)
		box.add(boton_parar)
		box.add(boton_grabar)
		box.add(boton_borrartodo)
		box.add(boton_borrartrozo)
		box.add(boton_crearvideo)
		box.add(boton_opciones)
		
		box2.pack_start (drawing_area, true, true, 0);
		box2.pack_start (box,false, false, 0);
		box2.pack_start (scale_1,false, true, 0);
		
		
		estado="STOP"
		bus = this.videopipeline.get_bus()
		bus.add_signal_watch()
		msg = bus.timed_pop_filtered (1000 * Gst.MSECOND,Gst.MessageType.STATE_CHANGED | Gst.MessageType.ERROR | Gst.MessageType.EOS );
		bus.message.connect(on_msg)
		
		bus2 = this.vaudiopipeline.get_bus()
		bus2.add_signal_watch()
		msg2 = bus2.timed_pop_filtered (1000 * Gst.MSECOND,Gst.MessageType.STATE_CHANGED | Gst.MessageType.ERROR | Gst.MessageType.EOS );
		bus2.message.connect(on_msg2)
		
	def Onpad (element:Element, zz:Pad)
		var opad = this.vaudiodecoder.get_static_pad("sink");
		zz.link(opad);


	def on_msg(m:Gst.Message)
		if m.type== Gst.MessageType.STATE_CHANGED
			old_state:Gst.State;
			new_state:Gst.State;
			pending_state:Gst.State;
			m.parse_state_changed (out old_state, out new_state, out pending_state);
			if (m.src == this.videosink) 
				// Remember whether we are in the PLAYING state or not:
				if (new_state == Gst.State.PLAYING)
					q : Gst.Query = new Gst.Query.seeking (Gst.Format.TIME);
					start:int64;
					end:int64;
					
					if ( this.videosink.query (q) ) 
						q.parse_seeking (null, out this.seek_enabled, out start, out end);
							
				if (new_state == Gst.State.READY)
					pass
				if (new_state == Gst.State.PAUSED)
					q : Gst.Query = new Gst.Query.seeking (Gst.Format.TIME);
					start:int64;
					end:int64;
					
					if ( this.videosink.query (q) ) 
						q.parse_seeking (null, out this.seek_enabled, out start, out end);
				

	def on_msg2(m:Gst.Message)
		if m.type== Gst.MessageType.STATE_CHANGED
			old_state:Gst.State;
			new_state:Gst.State;
			pending_state:Gst.State;
			m.parse_state_changed (out old_state, out new_state, out pending_state);
			if (m.src == this.vaudiosink) 
				// Remember whether we are in the PLAYING state or not:
				if (new_state == Gst.State.PLAYING)
					q : Gst.Query = new Gst.Query.seeking (Gst.Format.TIME);
					start:int64;
					end:int64;
					
					if ( this.vaudiosink.query (q) ) 
						q.parse_seeking (null, out this.seek_enabled2, out start, out end);
							
				if (new_state == Gst.State.READY)
					pass
				if (new_state == Gst.State.PAUSED)
					q : Gst.Query = new Gst.Query.seeking (Gst.Format.TIME);
					start:int64;
					end:int64;
					
					if ( this.vaudiosink.query (q) ) 
						q.parse_seeking (null, out this.seek_enabled2, out start, out end);
				
				
	def on_scale( )//cuando la escala se mueve por el usuario
		if seek_enabled
			if this.videopipeline.seek_simple(Gst.Format.TIME,  SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, (int64)(scale_1.get_value()*Gst.SECOND))
				pass
	
		if seek_enabled2
			if this.vaudiopipeline.seek_simple(Gst.Format.TIME,  SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, (int64)(scale_1.get_value()*Gst.SECOND))
				pass
			else
				print "audio no se movio"
		else
			print "audio no se puede mover"
	
	def on_tecla(evento:Gdk.EventKey):bool 
		if comienzo_grabacion!=-1 and archivo_abierto and estado=="PLAY" and (evento.str=="j" or evento.str=="J") // Jump to comienzo de grabacion
			if seek_enabled
				if this.videopipeline.seek_simple(Gst.Format.TIME,  SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, (int64)(comienzo_grabacion*Gst.SECOND))
					pass
			if seek_enabled2
				if this.vaudiopipeline.seek_simple(Gst.Format.TIME,  SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, (int64)(comienzo_grabacion*Gst.SECOND))
					pass
				else
					print "audio no se movio"
			else
				print "audio no se puede mover"
	
		return true
		
	def mover():bool // cambiando el valor mientras play el video.
		//desconectamos la deteccion de valores cambiados de la escala para evitar que el programa crea que el usuario esta cambiando el valor.
		
		scale_1.value_changed.disconnect(on_scale)
		var format = Gst.Format.TIME
		position=0
		if this.videopipeline.query_position(ref format, out position)
			scale_1.set_value(position/Gst.MSECOND/1000)
		duracion=0;
		if this.videopipeline.query_duration(ref format, out duracion)
			duracion=duracion/Gst.MSECOND/1000
			if estado!="STOP" do scale_1.set_range(0,duracion)
		//reactivamos la deteccion de valores cambiados de la escala para que se sepa que a partir de ahora es el usuario el que cambia el valor de la escala
		scale_1.value_changed.connect(on_scale)
		return true
	
	def on_realize()
		this.xid = (ulong)Gdk.X11Window.get_xid(this.drawing_area.get_window());
	
	
	def on_grabar (btn : Button)
		if (estado=="STOP" or estado=="PAUSE") and (archivo_abierto==true)
			boton_grabar.set_label(t.t("Parar"))
			boton_borrartodo.set_sensitive(false)
			boton_borrartrozo.set_sensitive(false)
			boton_grabar.set_sensitive(true)
			boton_crearvideo.set_sensitive(false)
			boton_play.set_sensitive(false)
			boton_parar.set_sensitive(false)
			boton_abrir.set_sensitive(false)
			
			var xoverlay = this.videosink as XOverlay;
			xoverlay.set_xwindow_id (this.xid);
			this.vaudiopipeline.set_state (State.PAUSED);
			this.videopipeline.set_state (State.PLAYING);
			this.audiopipeline.set_state (State.PLAYING);
			estado="REC"
			var format = Gst.Format.TIME
			position: float
			if this.videopipeline.query_position(ref format, out position)
				comienzo_grabacion=(position/Gst.MSECOND/1000)
			else
				comienzo_grabacion=-1
			
		else if estado=="REC" // parando la grabación
			boton_grabar.set_label(t.t("Grabar"))
			this.videopipeline.set_state (State.PAUSED);
			this.audiopipeline.set_state (State.PAUSED);
			this.audiopipeline.send_event(new Event.eos())
			this.audiopipeline.set_state (State.NULL);
			
			this.on_insertar()
			var xoverlay = this.videosink as XOverlay;
			xoverlay.set_xwindow_id (this.xid);
			this.vaudiopipeline.set_state (State.NULL);
			this.vaudiopipeline.set_state (State.READY);
			this.vaudiopipeline.set_state (State.PAUSED);
			scale_1.set_value(0)
			boton_borrartodo.set_sensitive(true)
			boton_borrartrozo.set_sensitive(true)
			boton_grabar.set_sensitive(true)
			boton_crearvideo.set_sensitive(true)
			boton_play.set_sensitive(true)
			boton_parar.set_sensitive(true)
			boton_abrir.set_sensitive(true)
			estado="PAUSE"
	
	def on_insertar()
		var iniciostr=this.comienzo_grabacion.to_string().replace(",",".")
		fin:float=0
		var format=Gst.Format.TIME
		if this.videopipeline.query_position(ref format, out fin)
			fin=fin/Gst.MSECOND/1000
		var finstr=fin.to_string().replace(",",".")
		print iniciostr
		print finstr
		
		if comienzo_grabacion!=-1 
			if combine
				try
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalA.wav trim =0 ="+iniciostr)
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalB.wav trim ="+iniciostr+" ="+finstr)
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalC.wav trim ="+finstr+" ="+duracion.to_string().replace(",","."))
					Process.spawn_command_line_sync ("sox -V1 --combine mix-power -v 1 "+Shell.quote(directorio_datos)+"grabacion.wav  -v 1 "+Shell.quote(directorio_datos)+"totalB.wav "+Shell.quote(directorio_datos)+"mix.wav")
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"totalA.wav "+Shell.quote(directorio_datos)+"mix.wav "+Shell.quote(directorio_datos)+"totalC.wav "+Shell.quote(directorio_datos)+"archivo_audio.wav")
				except
					print "error"
			else
				try
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalA.wav trim 0 ="+iniciostr)
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalB.wav trim ="+finstr+ " ="+duracion.to_string().replace(",","."))
					Process.spawn_command_line_sync ("sox -V1 "+Shell.quote(directorio_datos)+"totalA.wav "+Shell.quote(directorio_datos)+"grabacion.wav "+Shell.quote(directorio_datos)+"totalB.wav "+Shell.quote(directorio_datos)+"archivo_audio.wav")
				except
					print "error"
	
			
		
			
	def on_borrartrozo()
		if (archivo_abierto)
			dialogo_silenciar.show_all()
	
	def on_opciones()
		if (archivo_abierto)
			dialogo_opciones.show_all()
			
	def on_parar (btn :Button)
		if (estado=="PLAY" or estado=="PAUSE") and  (archivo_abierto==true)
			if this.videopipeline.seek_simple(Gst.Format.TIME,SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, 0)
				pass
			else 
				print "no ha podido parse"
			if this.vaudiopipeline.seek_simple(Gst.Format.TIME,SeekFlags.FLUSH| Gst.SeekFlags.ACCURATE, 0)
				pass
			else 
				print "audio no ha podido parse"
			this.videopipeline.set_state (State.PAUSED);
			this.vaudiopipeline.set_state (State.PAUSED);
			boton_play.set_label(t.t("Play"))
			boton_borrartodo.set_sensitive(true)
			boton_borrartrozo.set_sensitive(true)
			boton_grabar.set_sensitive(true)
			boton_crearvideo.set_sensitive(true)
			boton_abrir.set_sensitive(true)
			estado="STOP"
			
			
	def on_play (btn : Button)
		if (estado=="STOP" or estado=="PAUSE") and (archivo_abierto==true)
			btn.set_label(t.t("Pausar"))
			boton_borrartodo.set_sensitive(false)
			boton_borrartrozo.set_sensitive(false)
			boton_grabar.set_sensitive(false)
			boton_abrir.set_sensitive(false)
			boton_crearvideo.set_sensitive(false)
			var xoverlay = this.videosink as XOverlay;
			xoverlay.set_xwindow_id (this.xid);
			this.videopipeline.set_state (State.PLAYING);
			this.vaudiopipeline.set_state (State.PLAYING);
			estado="PLAY"
		else if (estado=="PLAY") and (archivo_abierto==true)
			btn.set_label(t.t("Play"))
			this.videopipeline.set_state (State.PAUSED);
			this.vaudiopipeline.set_state (State.PAUSED);
			boton_borrartodo.set_sensitive(true)
			boton_borrartrozo.set_sensitive(true)
			boton_grabar.set_sensitive(true)
			boton_crearvideo.set_sensitive(true)
			boton_abrir.set_sensitive(true)
			estado="PAUSE"
	
			
	def on_borrartodo()
		if (estado=="STOP" or estado=="PAUSE") and  (archivo_abierto==true)
			var xoverlay = this.videosink as XOverlay;
			xoverlay.set_xwindow_id (this.xid);
			
			this.vaudiopipeline.set_state (State.READY);
			this.vaudiopipeline.set_state (State.NULL);
			print duracion.to_string()
			try
				Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"silencio.wav "+Shell.quote(directorio_datos)+"archivo_audio.wav pad "+duracion.to_string().replace(",",".")+"@0")
				pass
			except
				pass
			this.videopipeline.set_state (State.PAUSED);
			this.vaudiopipeline.set_state (State.READY);
			this.vaudiopipeline.set_state (State.NULL);
			this.vaudiosrc.set ("location",directorio_datos+"archivo_audio.wav")
			this.vaudiopipeline.set_state (State.READY);
			this.vaudiopipeline.set_state (State.PLAYING);
			this.videopipeline.set_state (State.PLAYING);
			scale_1.set_value(0)
			boton_play.set_label(t.t("Pausar"))
			boton_borrartodo.set_sensitive(false)
			boton_borrartrozo.set_sensitive(false)
			boton_grabar.set_sensitive(false)
			boton_crearvideo.set_sensitive(false)
			boton_abrir.set_sensitive(false)
			boton_play.set_sensitive(true)
			boton_parar.set_sensitive(true)
			estado="PLAY"

	def on_crea_video()
		if (archivo_abierto)
			var FC=  new FileChooserDialog (t.t("Crear archivo:"), this, Gtk.FileChooserAction.SAVE,
			t.t("_Crear"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			FC.select_multiple = false;
			FC.set_modal(true)
			case FC.run ()
				when Gtk.ResponseType.CANCEL
					FC.hide()
					FC.close()
				when Gtk.ResponseType.ACCEPT
					FC.hide()
					try
						Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"archivo_audio.ogg")
						Process.spawn_command_line_sync ("avconv -i "+GLib.Shell.quote (nombre_archivo)+" -i "+Shell.quote(directorio_datos)+"archivo_audio.ogg -codec copy -map 0:0 -map 1:0 -y "+ FC.get_filename())
					except
						pass
		
	
		
		
	def on_abrir()
		var FC=  new FileChooserDialog (t.t("Abrir archivo:"), this, Gtk.FileChooserAction.OPEN,
			t.t("_Abrir"),Gtk.ResponseType.ACCEPT,
			t.t("_Cerrar"),Gtk.ResponseType.CANCEL);
			
		FC.select_multiple = false;
		FC.set_modal(true)
		case FC.run ()
			when Gtk.ResponseType.CANCEL
				FC.hide()
				FC.close()
			when Gtk.ResponseType.ACCEPT
				FC.hide()
				nombre_archivo=FC.get_filename ();
				this.videopipeline.set_state(State.READY)
				this.vaudiopipeline.set_state(State.PAUSED)
				this.vaudiopipeline.set_state(State.READY)
				this.vaudiopipeline.set_state(State.NULL)
				try
					Process.spawn_command_line_sync ("avconv -i "+GLib.Shell.quote (nombre_archivo)+" -codec copy -map 0:1  -y "+Shell.quote(directorio_datos)+"archivo_audio.ogg ")
					Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"archivo_audio.ogg "+Shell.quote(directorio_datos)+"archivo_audio.wav rate 44100")
					pass
				except
					pass
				this.videosrc.set("uri","file:///"+nombre_archivo);
				this.videosrc.set_property("volume", 0)
				var xoverlay = this.videosink as XOverlay;
				xoverlay.set_xwindow_id (this.xid);
				this.videopipeline.set_state(State.PLAYING)
				this.vaudiopipeline.set_state(State.PLAYING)
				estado="PLAY"
				archivo_abierto=true
				boton_play.set_label("Pausa")
				scale_1.set_visible(true)
				boton_borrartodo.set_sensitive(false)
				boton_borrartrozo.set_sensitive(false)
				boton_grabar.set_sensitive(false)
				boton_crearvideo.set_sensitive(false)
				
				boton_abrir.set_sensitive(false)
				boton_grabar.set_sensitive(false)
				boton_play.set_sensitive(true)
				boton_parar.set_sensitive(true)
	
	def verificar_directorio_usuario(dir:string)
		try
			nombre_programa:string="oggbikoiztu"
			var d=Dir.open( dir+"/")
			var encontrado=false
			var name=""
			while ( (name = d.read_name()) != null)
				if name=="."+nombre_programa
					encontrado=true
			
			if not encontrado
				DirUtils.create (dir+"/."+nombre_programa, 0700) 
			
			directorio_datos=dir+"/."+nombre_programa+"/"
			//copiar el archivo silencio.wav a la carpeta de datos siempre. Es un archivo infimo.
			Process.spawn_command_line_async ("cp /usr/share/oggbikoiztu/silencio.wav "+Shell.quote(directorio_datos))
			pass
		except
			print "error"
	
