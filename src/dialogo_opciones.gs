
class Dialogo_opciones:Gtk.Dialog
	combinar : Gtk.Label;
	combinar_sw : Gtk.Switch;
	hbox: Gtk.Box
	contenido: Gtk.Box
	init
		this.title = t.t("Aukerak");
		this.border_width = 5;
		this.set_default_size (350, 100);
		
		combinar = new Gtk.Label(t.t("Combinar audio de video con grabación:"));
		combinar_sw = new Gtk.Switch;
		
		hbox= new Gtk.Box (Gtk.Orientation.HORIZONTAL, 20);
		hbox.pack_start (combinar, false, true, 0);
		hbox.pack_start (combinar_sw, false, true, 0);
		
		contenido = get_content_area () as Gtk.Box;
		contenido.pack_start (hbox, false, true, 0);
		this.add_button (t.t("_Deacuerdo"), 0);
		combinar_sw.set_active(combine)
		
		// conectando las señales
		this.response.connect (on_response);
		
	def on_response(source:Gtk.Dialog, response_id:int)
		case (response_id)
			when 0
				this.hide()
				combine=combinar_sw.get_active()
		
