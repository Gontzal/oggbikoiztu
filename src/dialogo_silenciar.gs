uses Gtk,Gst

class Dialogo_silenciar:Gtk.Dialog
	lugar1 : Gtk.Entry ;
	lugar2 : Gtk.Entry ;
	desde : Gtk.Label;
	hasta : Gtk.Label;
	hbox: Gtk.Box
	contenido: Gtk.Box
	init
		this.title = t.t("Silenciar trozo");
		this.border_width = 5;
		this.set_default_size (350, 100);
		
		lugar1 = new Gtk.Entry ();
		lugar2 = new Gtk.Entry ();
		desde = new Gtk.Label(t.t("desde"));
		hasta = new Gtk.Label(t.t("hasta"));
		
		hbox= new Gtk.Box (Gtk.Orientation.HORIZONTAL, 20);
		hbox.pack_start (desde, false, true, 0);
		hbox.pack_start (lugar1, false, true, 0);
		hbox.pack_start (hasta, false, true, 0);
		hbox.pack_start (lugar2, false, true, 0);
		
		contenido = get_content_area () as Gtk.Box;
		contenido.pack_start (hbox, false, true, 0);
		this.add_button (t.t("_Silencia"), 0);
		this.add_button (t.t("_Cerrar"), 1);
		
		
		// conectando las señales
		this.response.connect (on_response);
		
	def on_response (source:Gtk.Dialog, response_id:int)
		case (response_id)
			when 0
				
				num1:double=double.parse(lugar1.get_text())
				num2:double=double.parse(lugar2.get_text())
				dura:double=num2-num1
				if num1<num2
					try
						Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalA.wav trim 0 ="+num1.to_string().replace(",","."))
						Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"archivo_audio.wav "+Shell.quote(directorio_datos)+"totalB.wav trim ="+num2.to_string().replace(",",".")+" ="+ventana_principal.duracion.to_string().replace(",",".") )
						Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"totalA.wav "+Shell.quote(directorio_datos)+"totalB.wav "+Shell.quote(directorio_datos)+"totalx.wav")
						Process.spawn_command_line_sync ("sox "+Shell.quote(directorio_datos)+"totalx.wav "+Shell.quote(directorio_datos)+"totaln.wav pad "+dura.to_string().replace(",",".")+"@"+num1.to_string().replace(",","."))
						Process.spawn_command_line_sync ("cp "+Shell.quote(directorio_datos)+"totaln.wav "+Shell.quote(directorio_datos)+"archivo_audio.wav")
					except
						pass
					ventana_principal.videopipeline.set_state (State.PAUSED);
					ventana_principal.vaudiopipeline.set_state (State.READY);
					ventana_principal.vaudiopipeline.set_state (State.NULL);
					ventana_principal.vaudiosrc.set ("location",directorio_datos+"archivo_audio.wav")
					ventana_principal.vaudiopipeline.set_state (State.READY);
					ventana_principal.vaudiopipeline.set_state (State.PLAYING);
					ventana_principal.videopipeline.set_state (State.PLAYING);
					ventana_principal.scale_1.set_value(0)
					ventana_principal.boton_play.set_label(t.t("Pausar"))
					ventana_principal.boton_borrartodo.set_sensitive(false)
					ventana_principal.boton_borrartrozo.set_sensitive(false)
					ventana_principal.boton_grabar.set_sensitive(false)
					ventana_principal.boton_abrir.set_sensitive(false)
					ventana_principal.boton_crearvideo.set_sensitive(false)
					ventana_principal.estado="PLAY"
					this.hide()
				else
					print "error"
			when 1
				this.hide()
		
